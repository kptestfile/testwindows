## Build Status
|               |           |
|:-------------:|:---------:|
| Build Status  |[![build](https://img.shields.io/gitlab/pipeline/kptestfile/testwindows.svg?job=job1)](https://gitlab.com/kptestfile/testwindows)|
| Pipeline|[![pipeline status](https://gitlab.com/gkp1998/Project/badges/master/pipeline.svg)](https://gitlab.com/gkp1998/project/pipelines)

# Diff



| __OS/Platform__| __Debug__ |   __Test__  | __Release__  |
|:-------------:|:---------:|:---------:|:-------:|
|Mac OS|